//
//  AppColor.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit

struct AppColor {

    static let Border        = UIColor(named: "Border")!
    static let Theme         = UIColor(named: "Theme")!
    static let Navigation    = UIColor(named: "Navigation")!
}
