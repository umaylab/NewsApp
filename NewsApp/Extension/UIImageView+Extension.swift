//
//  UIImageView+Extension.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit
import NetworkHelper

extension UIImageView {
  func getImage(with urlString: String,
                completion: @escaping (Result<UIImage, AppError>) -> ()) {

    let activityIndicator = UIActivityIndicatorView(style: .large)
    activityIndicator.color = .orange
    activityIndicator.startAnimating()
    activityIndicator.center = center
    addSubview(activityIndicator)

    guard let url = URL(string: urlString) else {
      completion(.failure(.badURL(urlString)))
      return
    }

    let request = URLRequest(url: url)

    NetworkHelper.shared.performDataTask(with: request) { [weak activityIndicator] (result) in
      DispatchQueue.main.async {
        activityIndicator?.stopAnimating()
      }
      switch result {
      case .failure(let appError):
        completion(.failure(.networkClientError(appError)))
      case .success(let data):
        if let image = UIImage(data: data) {
          completion(.success(image))
        }
      }
    }
  }
}


extension UIImageView {

    func sample(_ topic: String, _ subtopic: String, _ index: Int) {

        let size = CGSize(width: 1, height: 1)
        let placeholder = UIGraphicsImageRenderer(size: size).image { rendererContext in
            UIColor.lightGray.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }

        self.image = placeholder
    }
}
