//
//  Kategori.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import Foundation

struct Kategori: Decodable {
  let name: String
    
    static let lokalgroup = [
        Kategori(name: "general"),
        Kategori(name: "health"),
        Kategori(name: "science"),
        Kategori(name: "sports"),
        Kategori(name: "technology"),
        Kategori(name: "business"),

    ]
    
}
