//
//  News.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import Foundation


struct NewsSerach: Codable {
  let articles: [News]
}


struct News: Codable {
  let title: String?
  let description: String?
  let urlToImage: String!
  let author: String?
  let source: Source
  let url: String?
    
}


struct Source: Codable {
    let name: String?
}

struct RecipeSearch: Codable {
    let label: String
    let image: String
}
