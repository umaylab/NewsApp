//
//  NewsAPIClient.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import Foundation


import Foundation
import NetworkHelper


struct NewsAPIClient {
  static func fetchNews(with name: String, completion: @escaping (Result<[News], AppError>) -> ()) {
      let endpointURLString = "https://newsapi.org/v2/top-headlines?category=\(name)&apiKey=1be45d9cadc04864b6d35141b358a8c7"
    guard let url = URL(string: endpointURLString) else {
      completion(.failure(.badURL(endpointURLString)))
      return
    }
    let request = URLRequest(url: url)
    NetworkHelper.shared.performDataTask(with: request) { (result) in
      switch result {
      case .failure(let appError):
        completion(.failure(.networkClientError(appError)))
      case .success(let data):
        do {
          let newsSearch = try JSONDecoder().decode(NewsSerach.self, from: data)
          completion(.success(newsSearch.articles))
        } catch {
          completion(.failure(.decodingError(error)))
        }
      }
    }
  }
}



