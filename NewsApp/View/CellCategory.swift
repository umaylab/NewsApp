//
//  CellCategory.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit

class CellCategory: UICollectionViewCell {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var viewBottomLine: UIView!

    func bindData(title: String) {

        labelTitle.text = title
    }

    func set(isSelected: Bool = false) {

        viewBottomLine.isHidden = !isSelected
        labelTitle.textColor = isSelected ? AppColor.Theme : UIColor.label
    }
    
    
    public func configureCell(for kate: Kategori) {
        labelTitle.text = kate.name

    }
    
    
    
    
    
}
