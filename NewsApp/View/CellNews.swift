//
//  CellNews.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit


class CellNews: UITableViewCell {

    @IBOutlet var imageNews: UIImageView!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelAuthor: UILabel!
    @IBOutlet var buttonBookmark: UIButton!


    
    public func configureCell(for datanews: News) {
        labelTitle.text = datanews.title
        labelAuthor.text = datanews.description
        imageNews.getImage(with: datanews.urlToImage!) { [weak self] (result) in
        switch result {
        case .failure:
          break
        case .success(let image):
          DispatchQueue.main.async {
            self?.imageNews.image = image
          }
        }
      }
    }
    
    
}
