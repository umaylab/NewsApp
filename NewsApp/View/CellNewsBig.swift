//
//  CellNewsBig.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit


class CellNewsBig: UITableViewCell {

    @IBOutlet var labelIndex: UILabel!
    @IBOutlet var imageNews: UIImageView!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelDescription: UILabel!
    @IBOutlet var buttonMore: UIButton!

    override func awakeFromNib() {

        super.awakeFromNib()
        buttonMore.layer.borderWidth = 1
        buttonMore.layer.borderColor = AppColor.Border.cgColor
    }


    public func configureCell(for datanews: News) {
        labelTitle.text = datanews.title
        labelDescription.text = datanews.description
        labelDate.text = datanews.author
        //labelIndex.text = mobil.following
        imageNews.getImage(with: datanews.urlToImage!) { [weak self] (result) in
            switch result {
            case .failure:
                break
            case .success(let image):
                DispatchQueue.main.async {
                    self?.imageNews.image = image
                }
            }
        }
    }
}
