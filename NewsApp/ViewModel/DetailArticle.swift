//
//  DetailArticle.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit


class DetailArticle: UIViewController {

    @IBOutlet var imageNews: UIImageView!
//    @IBOutlet var labelCategory: UILabel!
//    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelTitle: UILabel!
//    @IBOutlet var imageUser: UIImageView!
//    @IBOutlet var labelUsername: UILabel!
//    @IBOutlet var labelLocation: UILabel!
    @IBOutlet var labelDescription: UILabel!
//    @IBOutlet var buttonMessage: UIButton!
//    @IBOutlet var viewMessageCount: UIView!
//    @IBOutlet var labelMessageCount: UILabel!


   // var detailArtikel: Podcast?
    var detailArtikel: News!

    static let identifier = "DetailArticle"

    override func viewDidLoad() {

        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.largeTitleDisplayMode = .never


        updateDetailUI()
    }





    @objc func actionFont(_ sender: UIButton) {

        print(#function)
    }


    @objc func actionBookmark(_ sender: UIButton) {

        print(#function)
    }


    @objc func actionShare(_ sender: UIButton) {

        print(#function)
    }


    func updateDetailUI() {
//        guard let detailArtikel = detailArtikel else {
//            fatalError("newsHeadline is nil, verify prepare(for segue: )")
//        }

        self.labelTitle.text = detailArtikel.title
        self.labelDescription.text = detailArtikel.description
        self.imageNews.getImage(with: detailArtikel.urlToImage!) { [weak self] (result) in
            switch result {
            case .failure:
                break
            case .success(let image):
                DispatchQueue.main.async {
                    self?.imageNews.image = image
                }
            }
        }



    }



}


extension DetailArticle: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if scrollView.tag == 11 {
            if let navController = navigationController as? NavigationController {
                if (scrollView.contentOffset.y > imageNews.frame.size.height/2) {
                    navController.setBackground(color: .systemBackground)
                } else {
                    navController.setBackground(color: .clear)
                }
            }
        }
    }
}
