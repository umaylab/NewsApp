//
//  NewsAppViewController.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit

class NewsAppViewController: UIViewController {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelSubTitle: UILabel!
    @IBOutlet var collectionViewMenu: UICollectionView!
    @IBOutlet var collectionViewProducts: UICollectionView!
    
    @IBOutlet var tableview: UITableView!
    
    
    private var selectedMenuIndex = 0
    
    private var kategoris = [Kategori]() {
      didSet {
          collectionViewMenu.reloadData()
      }
    }
    
    private var newsdata = [News]() {
      didSet {
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
      }
    }
    

    
    override func viewDidLoad() {

        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.largeTitleDisplayMode = .never

        
        labelTitle.text = "NewsApp"
        tableview.register(UINib(nibName: "CellNewsBig", bundle: Bundle.main), forCellReuseIdentifier: "CellNewsBig")
        tableview.register(UINib(nibName: "CellNews", bundle: Bundle.main), forCellReuseIdentifier: "CellNews")
        
        collectionViewMenu.register(UINib(nibName: "CellCategory", bundle: Bundle.main), forCellWithReuseIdentifier: "CellCategory")
        
        fetchGeneral()
    }
    


    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        if let navController = navigationController as? NavigationController {
            navController.setBackground(color: .clear)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {

        super.viewWillDisappear(animated)
        if let navController = navigationController as? NavigationController {
            navController.setNavigationBar()
        }
    }

    
    private func fetchGeneral(_ name: String = "general") {
        NewsAPIClient.fetchNews(with: name) { (result) in
          switch result {
          case .failure(let appError):
            print("error fetching news: \(appError)")
          case .success(let datanews):
            self.newsdata = datanews
          }
        }
      }
    

    private func fetchHealth(_ name: String = "health") {
        NewsAPIClient.fetchNews(with: name) { (result) in
          switch result {
          case .failure(let appError):
            print("error fetching news: \(appError)")
          case .success(let datanews):
            self.newsdata = datanews
          }
        }
      }

    private func fetchScience(_ name: String = "science") {
        NewsAPIClient.fetchNews(with: name) { (result) in
          switch result {
          case .failure(let appError):
            print("error fetching news: \(appError)")
          case .success(let datanews):
            self.newsdata = datanews
          }
        }
      }

    private func fetchSport(_ name: String = "sports") {
        NewsAPIClient.fetchNews(with: name) { (result) in
          switch result {
          case .failure(let appError):
            print("error fetching news: \(appError)")
          case .success(let datanews):
            self.newsdata = datanews
          }
        }
      }

    private func fetchTech(_ name: String = "technology") {
        NewsAPIClient.fetchNews(with: name) { (result) in
          switch result {
          case .failure(let appError):
            print("error fetching news: \(appError)")
          case .success(let datanews):
            self.newsdata = datanews
          }
        }
      }

    private func fetchBusiness(_ name: String = "business") {
        NewsAPIClient.fetchNews(with: name) { (result) in
          switch result {
          case .failure(let appError):
            print("error fetching news: \(appError)")
          case .success(let datanews):
            self.newsdata = datanews
          }
        }
      }
    
    

}



extension NewsAppViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return newsdata.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellNewsBig", for: indexPath) as? CellNewsBig else {
                fatalError("couldn't dequeue a HeadlineCell")
            }
            
            let datanews = newsdata[indexPath.row]
            cell.configureCell(for: datanews)
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellNews", for: indexPath) as? CellNews else {
            fatalError("couldn't dequeue a HeadlineCell")
        }
        
        let datanews = newsdata[indexPath.row]
        cell.configureCell(for: datanews)
        return cell
        
    }
}

extension NewsAppViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let detailXIB = DetailArticle(nibName: "DetailArticle", bundle: nil)
        detailXIB.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(detailXIB, animated: true)
        
        let detailPage = newsdata[indexPath.row]
        detailXIB.detailArtikel = detailPage

        print("detail tableview...")


    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if (indexPath.row == 0) { return 345 }
        return 110
    }
    
}




extension NewsAppViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return Kategori.lokalgroup.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCategory", for: indexPath) as! CellCategory

        let negara = Kategori.lokalgroup[indexPath.row]
        cell.configureCell(for: negara)
        cell.set(isSelected: (indexPath.row == selectedMenuIndex))
        return cell
        
        
        
        

    }
}



extension NewsAppViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

//        let width = (collectionView.frame.size.width-45)/2
       let height = collectionView.frame.size.height
        
        return CGSize(width: 80, height: height-20)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 15
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 20, left: 15, bottom: 0, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionViewMenu == collectionView) {
            selectedMenuIndex = indexPath.row
            collectionViewMenu.reloadItems(at: collectionViewMenu.indexPathsForVisibleItems)
            collectionViewMenu.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)

            if indexPath.row == 0 || indexPath.row == 7{
                fetchGeneral()
                print("detail 1 ...")

            }
            else if indexPath.row == 1 {

                fetchHealth()
                print("detail 3...")

            }
            else if indexPath.row == 2 {

                fetchScience()
                print("detail 4...")

            }
            else if indexPath.row == 3 {

                fetchSport()
                print("detail 5..")

            }
            else if indexPath.row == 4 {

                fetchTech()
                print("detail 6...")

            }
            else if indexPath.row == 5 {

                fetchBusiness()
                print("detail 7...")

            }
        }
        
        
    }
}



