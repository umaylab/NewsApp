//
//  ViewController.swift
//  NewsApp
//
//  Created by MacUmay on 04/10/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var imageViewLogo: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelSubTitle: UILabel!

    //-------------------------------------------------------------------------------------------------------------------------------------------
    override func viewDidLoad() {

        super.viewDidLoad()

        loadData()
    }

    // MARK: - Data methods
    //-------------------------------------------------------------------------------------------------------------------------------------------
    func loadData() {

        labelTitle.text = "Welcome to\nNewsAppTest"
        labelSubTitle.text = "See news today with\nnew update from media."
    }

    @IBAction func actionNews(_ sender: Any) {

        let masterXIB = NewsAppViewController()
        masterXIB.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(masterXIB, animated: true)

        print("masuk...")
    }
}
